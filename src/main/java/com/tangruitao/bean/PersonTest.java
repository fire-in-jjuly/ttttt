package com.tangruitao.bean;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 这个类的作者是13536
 * 开发日期:2021/12/15
 **/
public class PersonTest {

    public static void main(String[] args) {
        List<BlankCard> list=new ArrayList<>();
        list.add(new BlankCard(1111111,"招商银行"));
        list.add(new BlankCard(11111100,"建设银行"));
        Person person = new Person(4114222,"张三丰",list);
        List<BlankCard> list1=new ArrayList<>();
        list.add(new BlankCard(1111111,"招商银行"));
        list.add(new BlankCard(11111100,"建设银行"));
        Person person1 = new Person(4114222,"李四",list1);
        List<BlankCard> list3=new ArrayList<>();
        list.add(new BlankCard(1111111,"招商银行"));
        list.add(new BlankCard(11111100,"建设银行"));
        Person person3 = new Person(4114222,"张三丰",list3);
        List<Person> ll=new ArrayList<>();
        ll.add(person);
        ll.add(person1);
        ll.add(person3);
//        姓名
        ll.stream().forEach(s->System.out.print(s.getName()));
        System.out.println("===================================9");
//        获取姓名作为list集合
        List<String> collect = ll.stream().map(Person::getName).collect(Collectors.toList());
        collect.stream().forEach(System.out::print);
          ll.stream().forEach(s -> s.getName().equals("张三丰"));
    }

}
